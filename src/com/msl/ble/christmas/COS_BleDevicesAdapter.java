package com.msl.ble.christmas;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.msl.ble.christmas.R;

/** Adapter for holding devices found through scanning.
 *  Created by Kai on 2013-11-15.
 */
public class COS_BleDevicesAdapter {
    public final ArrayList<BluetoothDevice> leDevices;
    public final HashMap<BluetoothDevice, Integer> rssiMap = new HashMap<BluetoothDevice, Integer>();
    private Context con;
    
    public int maxSignal = 0;//the max signal of BLE-device;
    public int maxSignalBleNum = -1;
    public static int totalScanNum = 0;
    private final float LOWPASSFILTER = 0.3f;//coefficient for lowPassFilter of signal;
    
    //R.xml.preferences
    public static int FILTER_IN = -62;//IN value -70; pref
    public static int FILTER_OUT = -75;//TODO OUT value; should be out<in -80; pref
    public static boolean IfFlash = false;
    //public static boolean MODEL_DIR = false;
    
    //TODO å¦‚æžœBLE Deviceå¾ˆå¤šçš„è¯�ï¼Œç”¨ArrayListä»£æ›¿Array[];
    public int[] array_signalConstant = {-100,-100,-100,-100};
    public int[] array_signalLowPass = {-100,-100,-100,-100};
    public int[] array_signalAverage = {-100,-100,-100,-100};
    public int[][] arrayArray_signalConstant = {{-100,-100,-100,-100,-100,-100,-100,-100,-100,-100},{-100,-100,-100,-100,-100,-100,-100,-100,-100,-100},{-100,-100,-100,-100,-100,-100,-100,-100,-100,-100},{-100,-100,-100,-100,-100,-100,-100,-100,-100,-100}};//å½“æˆ�Queueï¼Œå…ˆè¿›å…ˆå‡º
    public int[] array_scanned_Number = {0,0,0,0};//å…¶å®žå°±æ˜¯rssiMap
    
    public static String[] BSSID = {"34:B1:F7:D5:55:FD","1c:ba:8c:20:c7:cf","1c:ba:8c:20:c8:13","1c:ba:8c:20:c9:c5"};
    //34:B1:F7:D5:55:FD
    //public final String[] BSSID = {"34:b1:f7:d5:5a:ee","1c:ba:8c:20:c7:cf","1c:ba:8c:20:c8:12","1c:ba:8c:20:c9:c5"};
    /*private final String BSSID_5aee = "34:b1:f7:d5:5a:ee";//ssid-address of BLE - sensorTag; []=BSSID
    private final String BSSID_c7cf = "1c:ba:8c:20:c7:cf";
    private final String BSSID_c812 = "1c:ba:8c:20:c8:12";
    private final String BSSID_c9c5 = "1c:ba:8c:20:c9:c5";*/

    //****************************************		
    public COS_BleDevicesAdapter(Context context) {
        leDevices = new ArrayList<BluetoothDevice>();
        con = context;
    }
    
    public void detectDevice(BluetoothDevice device, int rssi) {
    	if ( device.getName()==null || device.getName().length()<=0 || !device.getName().equals("SensorTag"))
    		return;
    	
    	if(device.getAddress().equalsIgnoreCase(BSSID[0])) {
    		array_signalLowPass[0] = (int) (LOWPASSFILTER*rssi + (1.0f-LOWPASSFILTER)*array_signalLowPass[0]);
    		//å¦‚æžœæ­¤æ—¶æ²¡æœ‰æ‰«æ��æ­¤BSSIDï¼Œä¸�æ‰§è¡Œæ­¤if()è¯­å�¥ï¼Œåˆ™arrayArray_signalConstantå�œæ­¢æ›´æ–°ï¼Œso array_signalAverageä¹Ÿå�œæ­¢è®¡ç®—
    		array_signalAverage[0] = getAverageAfterEnQueue(arrayArray_signalConstant[0], rssi);
    		array_signalConstant[0] = rssi;
    		array_scanned_Number[0] ++ ;
    	}
    		
    	else if(device.getAddress().equalsIgnoreCase(BSSID[1])) {
    		array_signalLowPass[1] = (int) (LOWPASSFILTER*rssi + (1.0f-LOWPASSFILTER)*array_signalLowPass[1]);
    		array_signalAverage[1] = getAverageAfterEnQueue(arrayArray_signalConstant[1], rssi);
    		array_signalConstant[1] = rssi;
    		array_scanned_Number[1] ++ ;
    	}
    		
    	else if(device.getAddress().equalsIgnoreCase(BSSID[2])) {
    		array_signalLowPass[2] = (int) (LOWPASSFILTER*rssi + (1.0f-LOWPASSFILTER)*array_signalLowPass[2]);
    		array_signalAverage[2] = getAverageAfterEnQueue(arrayArray_signalConstant[2], rssi);
    		array_signalConstant[2] = rssi;
    		array_scanned_Number[2] ++ ;
    	}
    		
    	else if(device.getAddress().equalsIgnoreCase(BSSID[3])) {
    		array_signalLowPass[3] = (int) (LOWPASSFILTER*rssi + (1.0f-LOWPASSFILTER)*array_signalLowPass[3]);
    		array_signalAverage[3] = getAverageAfterEnQueue(arrayArray_signalConstant[3], rssi);
    		array_signalConstant[3] = rssi;
    		array_scanned_Number[3] ++ ;
    	}
    	
    	MSLConfig.ifHasBleDevice = true;
    	
    	totalScanNum++;
    	
    	checkDeviceExist();
    }
    //å› ä¸ºBLE Scanningï¼Œä¸�åƒ�Wifiæ¯�æ¬¡æ‰«æ��å�¯ä»¥å¾—åˆ° å…¨éƒ¨å�¯è§�wifiçƒ­ç‚¹çš„ä¿¡æ�¯ï¼ŒBLEæ‰«æ��ä¸€æ¬¡å�ªèƒ½å¾—åˆ° ä¸€ä¸ª BLEè®¾å¤‡çš„ä¿¡æ�¯ï¼Œ
    //ï¼ˆæ¯”å¦‚ï¼Œè‹¥æœ‰4ä¸ªBLEè®¾å¤‡ï¼Œåˆ™éœ€è¦�è‡³å°‘4æ¬¡æ‰«æ��ï¼‰ã€‚æ‰€ä»¥è‹¥å­˜åœ¨BLEè®¾å¤‡ï¼Œåˆ™æ¯�æ¬¡æ‰«æ��æ—¶éƒ½å°†æ‰€æœ‰çš„BLEçš„Deviceçš„signalè®°å½•å‡�ä¸€ï¼Œ
    //ä»¥æ­¤å�˜ç›¸è¾¾åˆ°æ£€æµ‹æ˜¯å�¦å…¶ä»–BLE Deviceä»�ç„¶å­˜åœ¨æˆ–å·²ç»�offçš„æƒ…å†µ...å“‡å“ˆï¼Œé�žå¸¸è§„å“ˆï¼Œå¸…å“¥è�ªæ˜Žå�§ so excellent...
    //TODO å¦‚æžœè¦�æ£€æµ‹/æ•°æ�®åº“ä¸­çš„BLE Deviceå¾ˆå¤šï¼Œåˆ™ä¸�å�¯ä»¥ç”¨è¿™ç§�æ–¹æ³•ï¼Œå› ä¸ºé€ æˆ�è¯¯å·®è¾ƒå¤§ï¼Œæ­¤ç§�æƒ…å†µä¸‹å�¯ä»¥å»ºç«‹ä¸€ä¸ªArrayList<boolean> checkIfScannedForThisBLEDevice...
    public void checkDeviceExist() {
    	for(int i=0;i<array_signalLowPass.length;i++) {
    		array_signalLowPass[i]--;
    		if(array_signalLowPass[i]<-100)
    			array_signalLowPass[i] = -100;
    	}
    	for(int i=0;i<array_signalAverage.length;i++) {
    		array_signalAverage[i]--;
    		if(array_signalAverage[i]<-100)
    			array_signalAverage[i] = -100;
    	}
    }
    
    //print BLE-information in a String, to check the information; 
    public String getBLEInfo() {
    	String str_add = "";
    	String str_num = "";
    	String str_Constant = "";
    	String str_LowPass = "";
    	String str_Average = "";
    	String str;
    	for(int i=0;i<BSSID.length;i++) {
    		str_add = "beacon: " + "ee" + "       " + "cf" +  "       " + "12" + "       " + "c5";
    		
    		str_num = "number: " + array_scanned_Number[0] + "         " + array_scanned_Number[1] + "         " +
    				array_scanned_Number[2] + "         " + array_scanned_Number[3];
    		
    		str_Constant = "consta: " + array_signalConstant[0] + "     " + array_signalConstant[1] + "     " +
    				array_signalConstant[2] + "     " + array_signalConstant[3];
    		
    		str_LowPass = "lowpas: " + array_signalLowPass[0] + "     " + array_signalLowPass[1] + "     " +
    				array_signalLowPass[2] + "     " + array_signalLowPass[3];
    		
    		str_Average = "averag: " + array_signalAverage[0] + "     " + array_signalAverage[1] + "     " +
    				array_signalAverage[2] + "     " + array_signalAverage[3];
    	}
    	
    	str = str_add + "\n" + str_num + "\n" + str_Constant + "\n" + str_LowPass + "\n" + str_Average;
    	return str;
    }
    
    private int getAverageAfterEnQueue(int[] arr, int signal) {
    	int average = 0;
    	int sum = 0;
    	for(int i=arr.length-1;i>0;i--) {
    		arr[i] = arr[i-1];
    	}
    	arr[0] = signal;
    	for(int i=0;i<arr.length;i++) {
    		sum += arr[i];
    	}
    	average = sum / arr.length;
    	return average;
    }
    
    //*********
    //choose which device is supposed to show (here is signalLowPass);only filter_in is in use;
    //TODO make these getMaxSignalBleDevice...() one method, with boolean-mark lowpass or average, filterIn/OUT or just filterIN; 
    public int getMaxSignalBleDevice_LowPass() {
    	maxSignal = array_signalLowPass[0];
    	int num = 0;
    	for(int i=1;i<array_signalLowPass.length;i++) {
    		if(maxSignal<array_signalLowPass[i]) {
    			maxSignal = array_signalLowPass[i];
    			num = i;
    		}
    	}
    	if(maxSignal < FILTER_IN) {
    		return -1;
    	}
    	else {
    		return num;
    	}
    }
    //TODO use filter_IN and filter_OUT: "hard in, hard out"
    public int getMaxSignalBleDevice_LowPass(boolean mark) {
    	maxSignal = array_signalLowPass[0];
    	int num = 0;
    	for(int i=1;i<array_signalLowPass.length;i++) {
    		if(maxSignal<array_signalLowPass[i]) {
    			maxSignal = array_signalLowPass[i];
    			num = i;
    		}
    	}
    	if(maxSignal >= FILTER_IN) {
    		maxSignalBleNum = num;
    	}
    	else if(maxSignal<FILTER_OUT) {
    		maxSignalBleNum = -1;
    	}
    	//else if(maxSignal>=FILTER_OUT && maxSignal<FILTER_IN); //do nothing
    	return maxSignalBleNum;
    }
    public int getMaxSignalBleDevice_Average() {
    	maxSignal = array_signalAverage[0];
    	int num = 0;
    	for(int i=1;i<array_signalAverage.length;i++) {
    		if(maxSignal<array_signalAverage[i]) {
    			maxSignal = array_signalAverage[i];
    			num = i;
    		}
    	}
    	if(maxSignal < FILTER_IN) {
    		return -1;
    	}
    	else {
    		return num;
    	}
    }
    public int getMaxSignalBleDevice_Average(boolean mark) {
    	maxSignal = array_signalAverage[0];
    	int num = 0;
    	for(int i=1;i<array_signalAverage.length;i++) {
    		if(maxSignal<array_signalAverage[i]) {
    			maxSignal = array_signalAverage[i];
    			num = i;
    		}
    	}
    	if(maxSignal >= FILTER_IN) {
    		maxSignalBleNum = num;
    	}
    	else if(maxSignal<FILTER_OUT) {
    		maxSignalBleNum = -1;
    	}
    	//else if(maxSignal>=FILTER_OUT && maxSignal<FILTER_IN); //do nothing
    	return maxSignalBleNum;
    }
    //*********

    public void addDevice(BluetoothDevice device, int rssi) {
    	if ( device.getName()==null || device.getName().length()<=0 || !device.getName().equals("SensorTag"))
    		return;
    	
        if (!leDevices.contains(device)) {
            leDevices.add(device);
        }
        rssiMap.put(device, rssi);
    }
    
    public BluetoothDevice getDevice(int position) {
        return leDevices.get(position);
    }

    public void clear() {
        leDevices.clear();
    }

    /*public static int num_5aee = 0;//scanningNumber of BLE
	public static int num_c7cf = 0;
	public static int num_c812 = 0;
	public static int num_c9c5 = 0;*/
    
    /*public ArrayList<Integer> filter_51ee = iniArrayList(0,0,0,0,0);
    public ArrayList<Integer> filter_c7cf = iniArrayList(0,0,0,0,0);
    public ArrayList<Integer> filter_c812 = iniArrayList(0,0,0,0,0);
    public ArrayList<Integer> filter_c9c5 = iniArrayList(0,0,0,0,0);*/

    /*public ArrayList<Integer> iniArrayList (int AP1, int AP2, int AP3, int AP4, int AP5)
	{
		ArrayList<Integer> alist = new ArrayList<Integer> ();
		alist.add(0, AP1);
		alist.add(1, AP2);
		alist.add(2, AP3);
		alist.add(3, AP4);
		alist.add(4, AP5);
		return alist;
	}*/
    
    /*public void getIndoorPositionReferenceAuto_3Logitec () {
		
		boolean detect_logitec1 = false; //to detect if "logitec1 AP" can be detected by phone, which means if the "logitec1 AP" is visible or not
		boolean detect_logitec2 = false;
		boolean detect_logitec3 = false;
		boolean detect_AP = false; // to detect Pietro_phone
		boolean detect_tplink1 = false;
		boolean detect_tplink2 = false;
		
		BluetoothDevice sr;
		int i = 0;
		if(leDevices!=null) {
			int size = leDevices.size();
			for(i=0; i < size; i++) {
				sr = leDevices.get(i);
				if(sr.getAddress().equalsIgnoreCase(ble_sensortag_5aee)) {
					level_5aee = rssiMap.get(sr);
					filter_51ee.set(4, filter_51ee.get(3));
					filter_51ee.set(3, filter_51ee.get(2));
					filter_51ee.set(2, filter_51ee.get(1));
					filter_51ee.set(1, filter_51ee.get(0));
					filter_51ee.set(0, level_5aee);
					detect_logitec1 = true;
				}
				else if(sr.BSSID.equalsIgnoreCase(Logitec2)) {
					Logitec2_level = sr.level;
					filter_logitec2.set(4, filter_logitec2.get(3));
					filter_logitec2.set(3, filter_logitec2.get(2));
					filter_logitec2.set(2, filter_logitec2.get(1));
					filter_logitec2.set(1, filter_logitec2.get(0));
					filter_logitec2.set(0, Logitec2_level);
					detect_logitec2 = true;
				}
				else if(sr.BSSID.equalsIgnoreCase(Logitec3)) {
					Logitec3_level = sr.level;
					filter_logitec3.set(4, filter_logitec3.get(3));
					filter_logitec3.set(3, filter_logitec3.get(2));
					filter_logitec3.set(2, filter_logitec3.get(1));
					filter_logitec3.set(1, filter_logitec3.get(0));
					filter_logitec3.set(0, Logitec3_level);
					detect_logitec3 = true;
				}
				else if(sr.BSSID.equalsIgnoreCase(TPLink1)) {
					TPLink1_level = sr.level;
					filter_tplink1.set(4, filter_tplink1.get(3));
					filter_tplink1.set(3, filter_tplink1.get(2));
					filter_tplink1.set(2, filter_tplink1.get(1));
					filter_tplink1.set(1, filter_tplink1.get(0));
					filter_tplink1.set(0, TPLink1_level);
					detect_tplink1 = true;
				}
				else if(sr.BSSID.equalsIgnoreCase(TPLink2)) {
					TPLink2_level = sr.level;
					filter_tplink2.set(4, filter_tplink2.get(3));
					filter_tplink2.set(3, filter_tplink2.get(2));
					filter_tplink2.set(2, filter_tplink2.get(1));
					filter_tplink2.set(1, filter_tplink2.get(0));
					filter_tplink2.set(0, TPLink2_level);
					detect_tplink2 = true;
				}
				
				// detect Pietro_phone AndroidAP;
				else if(sr.SSID.equalsIgnoreCase(Pietro_phone)) {
					Pietro_level = sr.level;
					filter_pietro_phone.set(4, filter_pietro_phone.get(3));
					filter_pietro_phone.set(3, filter_pietro_phone.get(2));
					filter_pietro_phone.set(2, filter_pietro_phone.get(1));
					filter_pietro_phone.set(1, filter_pietro_phone.get(0));
					filter_pietro_phone.set(0, Pietro_level);
					detect_AP = true;
				}
			} // end for
		} //end if
		
		detect_Pietro_phone = detect_AP; // to show the Pietro_phone status
		
		Logitec1_level = ArrayListConvert(filter_logitec1, detect_logitec1, Logitec1_level);
		Logitec2_level = ArrayListConvert(filter_logitec2, detect_logitec2, Logitec2_level);
		Logitec3_level = ArrayListConvert(filter_logitec3, detect_logitec3, Logitec3_level);
		TPLink1_level = ArrayListConvert(filter_tplink1, detect_tplink1, TPLink1_level);
		TPLink2_level = ArrayListConvert(filter_tplink2, detect_tplink2, TPLink2_level);
		Pietro_level = ArrayListConvert(filter_pietro_phone, detect_AP, Pietro_level);
		
	}*/

/*public static int ArrayListConvert (ArrayList<Integer> alist, boolean detect, int instant)
{
	if(!detect) {
		alist.set(4, alist.get(3));
		alist.set(3, alist.get(2));
		alist.set(2, alist.get(1));
		alist.set(1, alist.get(0));
		alist.set(0, -100);
		instant = -100;
	}
	return instant;
}*/
    
    //********************************* Original
    /*@Override
    public int getCount() {
        return leDevices.size();
    }

    @Override
    public Object getItem(int i) {
        return leDevices.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        // General ListView optimization code.
        if (view == null) {
            view = inflater.inflate(R.layout.listitem_device, null);
            viewHolder = new ViewHolder();
            viewHolder.deviceAddress = (TextView) view.findViewById(R.id.device_address);
            viewHolder.deviceName = (TextView) view.findViewById(R.id.device_name);
            viewHolder.deviceRssi = (TextView) view.findViewById(R.id.device_rssi);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        BluetoothDevice device = leDevices.get(i);
        final String deviceName = device.getName();
        if (deviceName != null && deviceName.length() > 0)
            viewHolder.deviceName.setText(deviceName);
        else
            viewHolder.deviceName.setText("R.string.unknown_device");
        
        viewHolder.deviceAddress.setText(device.getAddress());
        viewHolder.deviceRssi.setText(""+rssiMap.get(device)+" dBm");

        return view;
    }

    private static class ViewHolder {
        TextView deviceName;
        TextView deviceAddress;
        TextView deviceRssi;
    }*/
}
