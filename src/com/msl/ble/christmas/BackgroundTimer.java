package com.msl.ble.christmas;

import java.util.TimerTask;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class BackgroundTimer extends TimerTask {
	String TAG = "ServerUploadTask";
	Context context;
	
	private COS_BleDevicesAdapter leDeviceListAdapter;
	private Scanner scanner;
	private BluetoothAdapter bluetoothAdapter;
	
	//private int SIGNAL = -62;
	private static int SCANPERIOD = 400;
	private String ibeacon4 = "34:B1:F7:D5:55:FD";//ibeacon with Zary
	private String ibeaconAnsel0 = "34:B1:F7:D5:59:22";
	
	//private boolean ifscanned = false;

	public BackgroundTimer(Context context,BluetoothAdapter bl) {
		this.context = context;
		this.bluetoothAdapter = bl;
		this.scanner = new Scanner(bluetoothAdapter, mLeScanCallback);
		Log.v("BackT", "++++++++++++++++++++++++++++");
	}

	public void run() {
		
		Log.v("BackT", ":::::)");
		
		/*if (scanner == null) {
            scanner = new Scanner(bluetoothAdapter, mLeScanCallback);
            scanner.startScanning();
        }*/
		
		MainActivity.current = false;
		MainActivity.rssiSignal = -100;
		scanner = new Scanner(bluetoothAdapter, mLeScanCallback);
        scanner.startScanning();
		
		/*Intent it = new Intent(this.context, COS_ChristmasTree_Video.class);
		it.putExtra("helpdrawable", 4);
		it.addCategory(Intent.CATEGORY_HOME);
		it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		this.context.startActivity(it);*/
		
		/*WakefulIntentService.acquireStaticLock(context, results); 
		Intent i = new Intent(context, ResultUploadService.class);
		context.startService(i);*/

	}// if network status
	
	//BLE scan
	//here is the signal-strength rssi; by Kai
    // Device scan callback.
    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {
                @Override
                public void onLeScan(final BluetoothDevice device, final int rssi, byte[] scanRecord) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                        	if(device.getAddress().equalsIgnoreCase(ibeaconAnsel0)) {
                        		MainActivity.rssiSignal = rssi;
                        		scanner.stopScanning();
                        		if(rssi>=COS_BleDevicesAdapter.FILTER_IN)
                        			MainActivity.current = true;
                        	}
                        	
                        	if( device.getAddress().equalsIgnoreCase(ibeaconAnsel0)) {
                        		if( MainActivity.current && !MainActivity.previous && !MainActivity.ifIsPlaying && (MainActivity.rssiSignal>=COS_BleDevicesAdapter.FILTER_IN)) {
                        			MainActivity.ifIsPlaying = true;
                        			
                        			Log.v("Test", "@@@@@@@@@@@@@" + device.getAddress());
                        		
                        			MainActivity.rssiSignal = -100;
                        		
                        			Intent it = new Intent(context, COS_ChristmasTree_Video.class);
                        			it.putExtra("helpdrawable", 4);
                        			it.addCategory(Intent.CATEGORY_HOME);
                        			it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        			context.startActivity(it);
                        			}
                        		else {
                        			//MainActivity.ifPlayed = false;
                        			}
                        	}
                        	
                            /*leDeviceListAdapter.addDevice(device, rssi);//also update rssi hashmap; by Kai
                            leDeviceListAdapter.detectDevice(device, rssi);*/
                        }
                    }).start();
                }
            };

    public static class Scanner extends Thread {
        private final BluetoothAdapter bluetoothAdapter;
        private final BluetoothAdapter.LeScanCallback mLeScanCallback;

        private volatile boolean isScanning = false;

        public Scanner(BluetoothAdapter adapter, BluetoothAdapter.LeScanCallback callback) {
            bluetoothAdapter = adapter;
            mLeScanCallback = callback;
        }

        public boolean isScanning() {
            return isScanning;
        }

        public void startScanning() {
            synchronized (this) {
                isScanning = true;
                start();
            }
        }

        public void stopScanning() {
            synchronized (this) {
                isScanning = false;
                bluetoothAdapter.stopLeScan(mLeScanCallback);
            }
        }
        
        @Override
        public void run() {
            try {
                    synchronized (this) {
                        bluetoothAdapter.startLeScan(mLeScanCallback);
                        }
                    sleep(SCANPERIOD);
                    synchronized (this) {
                        bluetoothAdapter.stopLeScan(mLeScanCallback);
                        //TODO
                        MainActivity.previous = MainActivity.current;
                        }
                    }
            catch (InterruptedException ignore) { } 
            finally { bluetoothAdapter.stopLeScan(mLeScanCallback); }
        }

        /*@Override
        public void run() {
            try {
                while (true) {
                	
                    synchronized (this) {
                        if (!isScanning)
                            break;
                        bluetoothAdapter.startLeScan(mLeScanCallback);
                        }
                    sleep(500);
                    
                    synchronized (this) {
                        bluetoothAdapter.stopLeScan(mLeScanCallback);
                    }
                }
                
            } 
            catch (InterruptedException ignore) { } 
            finally { bluetoothAdapter.stopLeScan(mLeScanCallback); }
        }*/
        //å�¦ä¸€ä¸ªrun()çš„æ€�æƒ³æ˜¯: startLeScan();while(isScanning){sleep(SCAN_PERIOD);} stopLeScan();
    }//end Scanner thread
	//***

}
