package com.msl.ble.christmas;

import java.util.TimerTask;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;

public class ScheduleTimerTask extends TimerTask {
	
	//Handler; Message; Looper
	
	Context context;
	String TAG = "ScheduleTimerTask";
	Handler handler;
	Activity ac;
	Message msg;
	
	public ScheduleTimerTask() {
		super();
	}
	
	public ScheduleTimerTask(Context context) {
		super();
		this.context = context;
	}
	
	public ScheduleTimerTask(Handler mhandler, Activity ap) {
		super();
		this.handler = mhandler;
		this.ac = ap;
	}
	
	@Override
	public void run() {
		//MSLConfig.ifHasBleDevice = MSLConfig.ifflag;//to check if there is ble-scanning event (which is if there is BLE-device activated)
		
		//if there is BLE scanning activity...
		if(MSLConfig.ifHasBleDevice) {
			msg = new Message();
			msg.what = 1;
			//msg.obj = MSLConfig.ifHasBleDevice;
			handler.sendMessage(msg);
		}
		//if there is not BLE scanning activity...
		else if(!MSLConfig.ifHasBleDevice && MSLConfig.ifStart) {
			msg = new Message();
			msg.what = 2;
			//msg.obj = MSLConfig.ifHasBleDevice;
			handler.sendMessage(msg);
		}
		
		//æ¯�æ¬¡åœ¨run()æ–¹æ³•çš„æœ€å�Žå°†ifflagç½®ä¸ºflase,åœ¨ä¸‹æ¬¡run()æ‰§è¡Œæ—¶(1så�Ž)ï¼Œåœ¨ä¸‹ä¸€ä¸ªscanä¸­(å°�äºŽ1s)å°†å…¶ç½®ä¸ºtrueï¼Œä»Žè€Œè¾¾åˆ°æ£€æµ‹æ˜¯å�¦æœ‰BLE deviceåœ¨è¢«scanï¼Œå“¥å¸…å�§
		//everytime in run(),make ifflag flase, so by the next time of run() is executing, make ifflag true, to check if it is scanning...
		Log.v("blerssi", " - " + MSLConfig.ifHasBleDevice + " " + MSLConfig.ifStart);
		MSLConfig.ifHasBleDevice = false;
	}
}
