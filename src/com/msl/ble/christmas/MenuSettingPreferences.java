package com.msl.ble.christmas;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.PreferenceActivity;
import android.preference.Preference;

@SuppressLint("NewApi")
public class MenuSettingPreferences extends PreferenceActivity {

	private String TAG = "MenuSettingPreferences";
    @SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        
        CheckBoxPreference FlashLight = (CheckBoxPreference)getPreferenceManager().findPreference("pref_flash");
        FlashLight.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {            
        	public boolean onPreferenceChange(Preference preference, Object newValue) {
        		COS_BleDevicesAdapter.IfFlash = (Boolean) newValue;
        		return true;
        		}
        	});//listener
        EditTextPreference FILTER_IN = (EditTextPreference)getPreferenceManager().findPreference("pref_ble_filter_in");
        FILTER_IN.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {            
        	public boolean onPreferenceChange(Preference preference, Object newValue) {
        		COS_BleDevicesAdapter.FILTER_IN = Integer.parseInt((String)newValue);
        		return true;
        		}
        	});//listener
        EditTextPreference FILTER_OUT = (EditTextPreference)getPreferenceManager().findPreference("pref_ble_filter_out");
        FILTER_OUT.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {            
        	public boolean onPreferenceChange(Preference preference, Object newValue) {
        		COS_BleDevicesAdapter.FILTER_OUT = Integer.parseInt((String)newValue);
        		return true;
        		}
        	});//listener
        
    }
}
    
/*	private static int prefs=R.xml.preferences;
    @Override
    protected void onCreate(final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        try {
            getClass().getMethod("getFragmentManager");
            AddResourceApi11AndGreater();
        } catch (NoSuchMethodException e) { //Api < 11
            AddResourceApiLessThan11();
        }
    }
    protected void AddResourceApiLessThan11()
    {
        addPreferencesFromResource(prefs);
    }

    protected void AddResourceApi11AndGreater()
    {
        getFragmentManager().beginTransaction().replace(android.R.id.content, new PF()).commit();
    }
    public static class PF extends PreferenceFragment
    {       
        @Override
        public void onCreate(final Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(SettingPreferences.prefs); //outer class private members seem to be visible for inner class, and making it static made things so much easier
        }
    }*/


