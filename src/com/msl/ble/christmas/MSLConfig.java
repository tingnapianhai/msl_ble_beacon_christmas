package com.msl.ble.christmas;

import java.util.ArrayList;
import java.util.HashMap;

import android.bluetooth.BluetoothDevice;

public class MSLConfig {
	//***
	public static boolean ifStart = false;
	public static int BLE_Dir = 0; //-1:left; 0:middle; 1:right; 
	public final int MESSAGE_BLE_SCANNING = 1;
	public final int MESSAGE_BLE_NOSCANNING = 2;
	
	public static boolean ifHasBleDevice = false;
	
    //***
	public static ArrayList<Float> x_pointer_array = initializeArrayList(0,0,0,0,0,0,0,0,0,0);
	public static ArrayList<Float> y_pointer_array = initializeArrayList(0,0,0,0,0,0,0,0,0,0);
	public static ArrayList<Float> initializeArrayList (float AP1, float AP2, float AP3, float AP4, float AP5, float AP6, float AP7, float AP8, float AP9, float AP10)
	{
		ArrayList<Float> alist = new ArrayList<Float> ();
		alist.add(0, AP1);
		alist.add(1, AP2);
		alist.add(2, AP3);
		alist.add(3, AP4);
		alist.add(4, AP5);
		alist.add(5, AP6);
		alist.add(6, AP7);
		alist.add(7, AP8);
		alist.add(8, AP9);
		alist.add(9, AP10);
		return alist;
	}
	
	public static ArrayList<Float> ArrayListConvert (ArrayList<Float> alist, float value)
	{
		alist.set(9, alist.get(8));
		alist.set(8, alist.get(7));
		alist.set(7, alist.get(6));
		alist.set(6, alist.get(5));
		alist.set(5, alist.get(4));
		
		alist.set(4, alist.get(3));
		alist.set(3, alist.get(2));
		alist.set(2, alist.get(1));
		alist.set(1, alist.get(0));
		alist.set(0, value);
		return alist;
	}
	
}