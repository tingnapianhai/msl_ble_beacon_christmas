/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.msl.ble.christmas;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Timer;

import org.json.JSONObject;

import com.msl.ble.christmas.COS_ChristmasTree.Scanner;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;
/**
 * Activity for scanning and displaying available Bluetooth LE devices.
 */
public class MainActivity extends Activity {

    private static final int REQUEST_ENABLE_BT = 1;
    private static final long SCAN_PERIOD = 500;
    private static boolean hasBackgroundThread = false;

    private BluetoothAdapter bluetoothAdapter;
    
    Timer backgroundTimer = new Timer();
    static boolean ifIsPlaying = false;
    static boolean isPlayed = false;
    static int rssiSignal = -100;
    
    static boolean previous = false;
    static boolean current = false;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //getActionBar().setTitle("Device scan");
        
        setContentView(R.layout.msl_christmas_video);
        preferenceInitialize();
        
        //TODO to get BSSID of BLE-SensorTag
        /*new Thread() {
			public void run() {
				String str = "none";
				try { 
					str = sendConnectGet("http://msl1.it.kth.se/contentmanager/posters/3.json");
					JSONObject jobj = new JSONObject(str);
					COS_BleDevicesAdapter.BSSID[0] = jobj.getString("beacon").substring(0, 17);
					Log.v("blejson", "222 " + COS_BleDevicesAdapter.BSSID[0]);
					}
				catch (Exception e) {e.printStackTrace();}
				}
			}.start();*/
        
        // Use this check to determine whether BLE is supported on the device.  Then you can
        // selectively disable BLE-related features.
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, "R.string.ble_not_supported", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        // Initializes a Bluetooth adapter.  For API level 18 and above, get a reference to
        // BluetoothAdapter through BluetoothManager.
        final BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        bluetoothAdapter = bluetoothManager.getAdapter();

        // Checks if Bluetooth is supported on the device.
        if (bluetoothAdapter == null) {
            Toast.makeText(this, "R.string.error_bluetooth_not_supported", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
        
        if(!hasBackgroundThread) {
        	Thread WatchdogThread = new Thread(null, RunnableWatchdog, "watchdog_thread");
        	WatchdogThread.start();
        	hasBackgroundThread = true;
        	//Toast.makeText(this, "App started successfully", Toast.LENGTH_SHORT).show();
        }
        
        SystemClock.sleep(3000);
		this.finish();//TODO
        
    }//onCreate
    
    @Override
    protected void onResume() {
        super.onResume();

        // Ensures Bluetooth is enabled on the device.  If Bluetooth is not currently enabled,
        // fire an intent to display a dialog asking the user to grant permission to enable it.
        if (!bluetoothAdapter.isEnabled()) {
            final Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            return;
        }

        init();
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	getMenuInflater().inflate(R.menu.gatt_scan, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.pref:
    			Intent intent = new Intent(MainActivity.this, MenuSettingPreferences.class);
    			startActivityForResult(intent, 100);
    			return true;
        }
        return true;
    }
    
    @Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
    	
    	if(keyCode == KeyEvent.KEYCODE_BACK)
        {
            Log.d("Test", "Back button pressed!");
        }
        else if(keyCode == KeyEvent.KEYCODE_HOME)
        {
            Log.d("Test", "Home button pressed!");
        }
    	Log.d("Test", "******************");
		return super.onKeyDown(keyCode, event);
	}

	@Override
    protected void onPause() {
        super.onPause();
    }
    
    @Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
    	//this.finish();
		super.onBackPressed();
	}

	@Override
	protected void onDestroy() {
		//this.finish();
		Log.v("BackT", "Destroy ---");
		super.onDestroy();
	}

	private void init() {
        invalidateOptionsMenu();
    }
	
    //*****
    private void preferenceInitialize() {
		PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
		SharedPreferences p = PreferenceManager.getDefaultSharedPreferences(this);
		try {
			COS_BleDevicesAdapter.IfFlash = p.getBoolean("pref_flash", COS_BleDevicesAdapter.IfFlash);
			COS_BleDevicesAdapter.FILTER_IN = Integer.parseInt(p.getString("pref_ble_filter_in", ""+COS_BleDevicesAdapter.FILTER_IN));
			COS_BleDevicesAdapter.FILTER_OUT = Integer.parseInt(p.getString("pref_ble_filter_out", ""+COS_BleDevicesAdapter.FILTER_OUT));
			}
		catch (Exception e) {e.printStackTrace();}
	}
    
    public String sendConnectGet(String url0) throws Exception {
		try{
			URL obj = new URL(url0);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("GET");
			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'GET' request to URL : " + url0);
			System.out.println("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			return response.toString();
		}catch(Exception ex){
			return "";
		}
	}
    //*****************
    private Runnable RunnableWatchdog = new Runnable() {
		public void run() {
			backgroundWatchdog();
			//finish();
		}
	};
	
	private void backgroundWatchdog(){
		
		PowerManager pm = (PowerManager)getSystemService(Context.POWER_SERVICE); 
		WakeLock wakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK |PowerManager.ACQUIRE_CAUSES_WAKEUP, "MyWakeLock"); // for Samsung
		//WakeLock wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyWakeLock"); //for others, Google HTC //å±�å¹•è‡ªåŠ¨ä¼šå…³é—­
		wakeLock.acquire(3*7*24*3600*1000);
		//wakeLock.acquire();
			
		BackgroundTimer uploadtask = new BackgroundTimer(this,bluetoothAdapter);
		backgroundTimer.scheduleAtFixedRate(uploadtask, 4000, 500);//TODO
		
		//backgroundTimer.schedule(uploadtask, 5000);
		
		/*Intent i = new Intent(MainActivity.this, BackgroundService.class);
		startService(i);*/

		/*SystemClock.sleep(7*24*3600*1000);
		
		// to make sure that the last records are uploaded to the server
		uploadtask.run();
		backgroundTimer.cancel();
		backgroundTimer = null;
		
		//wakeLock.release();
		finish();*/
	}
    
}