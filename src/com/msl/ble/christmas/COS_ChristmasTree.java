/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.msl.ble.christmas;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Timer;

import org.json.JSONObject;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;
/**
 * Activity for scanning and displaying available Bluetooth LE devices.
 */
public class COS_ChristmasTree extends Activity {

    private static final int REQUEST_ENABLE_BT = 1;
    private static final long SCAN_PERIOD = 500;

    private COS_BleDevicesAdapter leDeviceListAdapter;
    private BluetoothAdapter bluetoothAdapter;
    private Scanner scanner;
    
    int curr_BLE_pre = -1;
    int curr_BLE = -1;
    public static final int EDIT_PREFS = 100;
    
    private WebView wv;
    private VideoView vv;
	private LinearLayout midsection;
	
	//Christmas-tree
	AudioManager audioManager;
	MediaPlayer mMediaPlayer;
	Context con;
	private static int originalVolume = 0;
  //******************************************
    private Handler ble_handler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case 1:
				curr_BLE = leDeviceListAdapter.getMaxSignalBleDevice_LowPass(true);
				if(curr_BLE!=curr_BLE_pre) {
					//ible_ibt0.setText(leDeviceListAdapter.maxSignal+"  "+curr_BLE);
					//setColor(curr_BLE);
					setBackgroundImage(curr_BLE);
				}
				
				curr_BLE_pre = curr_BLE;
				//ible_ibt0.setText(leDeviceListAdapter.maxSignal+"  "+curr_BLE);
				MSLConfig.ifStart = true;//start/stop thread å¦‚æžœè“�ç‰™æ²¡æ‰“å¼€ï¼Œæœ‰äº›åˆ�å§‹åŒ–ä¸�ä¼šå®Œæˆ�
				break;
			case 2:
				leDeviceListAdapter.maxSignalBleNum = -1;//
				curr_BLE = -1;
				if(curr_BLE!=curr_BLE_pre) {
					//ible_ibt0.setText("  -1");
					//setColor(curr_BLE);
					setBackgroundImage(curr_BLE);
				}
				curr_BLE_pre = curr_BLE;
				break;
			}
			super.handleMessage(msg);
		}
	};
    //for timerTask
    Timer scheduleTimer = new Timer();
	ScheduleTimerTask schedule_timerTask;
	private Runnable RunnableTask = new Runnable() {
		public void run() {
			backgroundTask();
		}
	};
	private void backgroundTask() {
		scheduleTimer.cancel();
		scheduleTimer.purge();
		scheduleTimer = new Timer();
		schedule_timerTask = new ScheduleTimerTask(ble_handler,this);
		scheduleTimer.schedule(schedule_timerTask, 0, 500); //TODO
	}
	Thread timerThread = new Thread(null, RunnableTask, "timerTask_thread");
	
	/*private Button ible_ibt0;//represents each point
	private Button ible_ibt1;
	private Button ible_ibt2;
	private Button ible_ibt3;*/
	//******************************************

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //getActionBar().setTitle("Device scan");
        
        setContentView(R.layout.msl_christmas_video);
        preferenceInitialize();
        
        
        //TODO to get BSSID of BLE-SensorTag
        new Thread() {
			public void run() {
				String str = "none";
				try { 
					str = sendConnectGet("http://msl1.it.kth.se/contentmanager/posters/3.json");
					JSONObject jobj = new JSONObject(str);
					COS_BleDevicesAdapter.BSSID[0] = jobj.getString("beacon").substring(0, 17);
					Log.v("blejson", "222 " + COS_BleDevicesAdapter.BSSID[0]);
					}
				catch (Exception e) {e.printStackTrace();}
				}
			}.start();
        
        //for Christmas-tree
        con = this;
		mMediaPlayer = new MediaPlayer();
		audioManager = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
		audioManager.setMode(AudioManager.MODE_IN_CALL); //if no this, it wont disable speaker model
		//originalVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC); //recording original volume of device
		
		int max_volume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, max_volume, 0);
		audioManager.setSpeakerphoneOn(true);
		
        /*ible_ibt0 = (Button)findViewById(R.id.ible_sample_ibt0);
        ible_ibt1 = (Button)findViewById(R.id.ible_sample_ibt1);
        ible_ibt2 = (Button)findViewById(R.id.ible_sample_ibt2);
        ible_ibt3 = (Button)findViewById(R.id.ible_sample_ibt3);*/
        
        midsection = (LinearLayout)findViewById(R.id.midsection);
        
        //VideoView
        vv = new VideoView(this);
        vv.setVideoPath("/mnt/sdcard/tree/Jensvideo.mp4");//from local file
		//vv.setVideoURI(Uri.parse("http://msl1.it.kth.se/videos/JensHoHoHo.mp4"));
		vv.setMediaController(new MediaController(this));
		vv.requestFocus();
		midsection.addView(vv);
        vv.setVisibility(View.INVISIBLE);
        
        // Use this check to determine whether BLE is supported on the device.  Then you can
        // selectively disable BLE-related features.
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, "R.string.ble_not_supported", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        // Initializes a Bluetooth adapter.  For API level 18 and above, get a reference to
        // BluetoothAdapter through BluetoothManager.
        final BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        bluetoothAdapter = bluetoothManager.getAdapter();

        // Checks if Bluetooth is supported on the device.
        if (bluetoothAdapter == null) {
            Toast.makeText(this, "R.string.error_bluetooth_not_supported", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
        
        timerThread.start();
        
    }//onCreate
    
    private void stopTimerThread () {
    	if(schedule_timerTask!=null) {
    		schedule_timerTask.cancel();
    		schedule_timerTask = null;
			}
    	
    	if(scheduleTimer!=null) {
    		scheduleTimer.cancel();
    		scheduleTimer = null;
			}
		/*scheduleTimer.cancel();
		scheduleTimer.purge();
		scheduleTimer = null;
		schedule_timerTask.cancel();
		schedule_timerTask = null;
		timerThread.interrupt();//Thread.currentThread().interrupt();
		timerThread = null;*/
	}

    //decide which one should be shown on the title - "STOP", "SCAN"; by Kai
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	getMenuInflater().inflate(R.menu.gatt_scan, menu);
        if (scanner == null || !scanner.isScanning()) {
            menu.findItem(R.id.menu_stop).setVisible(false);
            menu.findItem(R.id.menu_scan).setVisible(true);
            menu.findItem(R.id.menu_refresh).setActionView(null);
        } else {
            menu.findItem(R.id.menu_stop).setVisible(true);
            menu.findItem(R.id.menu_scan).setVisible(false);
            menu.findItem(R.id.menu_refresh).setActionView(
                    R.layout.actionbar_indeterminate_progress);
        }
        return true;
    }

    //press "SCAN" or "STOP" on the top title area; by Kai
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_scan:
                leDeviceListAdapter.clear();
                if (scanner == null) {
                    scanner = new Scanner(bluetoothAdapter, mLeScanCallback);
                    scanner.startScanning();

                    invalidateOptionsMenu();
                }
                break;
            case R.id.menu_stop:
                if (scanner != null) {
                    scanner.stopScanning();
                    scanner = null;

                    invalidateOptionsMenu();
                }
                break;
            case R.id.pref:
    			Intent intent = new Intent(COS_ChristmasTree.this, MenuSettingPreferences.class);
    			startActivityForResult(intent, EDIT_PREFS);
    			return true;
        }
        return true;
    }
    
    //at the first time to aks if user want to enable Bluetooth; by Kai
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // User chose not to enable Bluetooth.
        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == Activity.RESULT_CANCELED) {
            	Log.v("bleask", "NO");
                finish();
            } 
            else {
            	Log.v("bleask", "YES");
                init();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
    
    @Override
    protected void onResume() {
        super.onResume();

        // Ensures Bluetooth is enabled on the device.  If Bluetooth is not currently enabled,
        // fire an intent to display a dialog asking the user to grant permission to enable it.
        if (!bluetoothAdapter.isEnabled()) {
            final Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            return;
        }

        init();
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (scanner != null) {
            scanner.stopScanning();
            scanner = null;
        }
    }
    
    @Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
    	stopTimerThread();
    	this.finish();
		super.onBackPressed();
	}

	@Override
	protected void onDestroy() {
		stopTimerThread();
		this.finish();
		super.onDestroy();
	}

	private void init() {
        if (leDeviceListAdapter == null) {
            leDeviceListAdapter = new COS_BleDevicesAdapter(getBaseContext());
            //setListAdapter(leDeviceListAdapter);
        }

        if (scanner == null) {
            scanner = new Scanner(bluetoothAdapter, mLeScanCallback);
            scanner.startScanning();
        }

        invalidateOptionsMenu();
    }

    //here is the signal-strength rssi; by Kai
    // Device scan callback.
    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {
                @Override
                public void onLeScan(final BluetoothDevice device, final int rssi, byte[] scanRecord) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            leDeviceListAdapter.addDevice(device, rssi);//also update rssi hashmap; by Kai
                            leDeviceListAdapter.detectDevice(device, rssi);
                        }
                    });
                }
            };

    public static class Scanner extends Thread {
        private final BluetoothAdapter bluetoothAdapter;
        private final BluetoothAdapter.LeScanCallback mLeScanCallback;

        private volatile boolean isScanning = false;

        public Scanner(BluetoothAdapter adapter, BluetoothAdapter.LeScanCallback callback) {
            bluetoothAdapter = adapter;
            mLeScanCallback = callback;
        }

        public boolean isScanning() {
            return isScanning;
        }

        public void startScanning() {
            synchronized (this) {
                isScanning = true;
                start();
            }
        }

        public void stopScanning() {
            synchronized (this) {
                isScanning = false;
                bluetoothAdapter.stopLeScan(mLeScanCallback);
            }
        }

        @Override
        public void run() {
            try {
                while (true) {
                    synchronized (this) {
                        if (!isScanning)
                            break;

                        bluetoothAdapter.startLeScan(mLeScanCallback);
                    }

                    sleep(SCAN_PERIOD);

                    synchronized (this) {
                        bluetoothAdapter.stopLeScan(mLeScanCallback);
                    }
                }
            } catch (InterruptedException ignore) {
            } finally {
                bluetoothAdapter.stopLeScan(mLeScanCallback);
            }
        }
        
        //å�¦ä¸€ä¸ªrun()çš„æ€�æƒ³æ˜¯: startLeScan();while(isScanning){sleep(SCAN_PERIOD);} stopLeScan();
        
    }//end Scanner thread
    
    //*****
    private void preferenceInitialize() {
		PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
		SharedPreferences p = PreferenceManager.getDefaultSharedPreferences(this);
		try {
			COS_BleDevicesAdapter.IfFlash = p.getBoolean("pref_flash", COS_BleDevicesAdapter.IfFlash);
			COS_BleDevicesAdapter.FILTER_IN = Integer.parseInt(p.getString("pref_ble_filter_in", ""+COS_BleDevicesAdapter.FILTER_IN));
			COS_BleDevicesAdapter.FILTER_OUT = Integer.parseInt(p.getString("pref_ble_filter_out", ""+COS_BleDevicesAdapter.FILTER_OUT));
			}
		catch (Exception e) {e.printStackTrace();}
	}
    private void setBackgroundImage (int num) {
    	switch (num) {
    		case -1:
    			//change-View
    			/*if(!vv.isPlaying()) {
    				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    				midsection.removeAllViews();
    				wv = new WebView(this);
    				wv.getSettings().setJavaScriptEnabled(true);
    				wv.setWebViewClient(new WebViewClient() {
    					public boolean shouldOverrideUrlLoading(WebView view, String url) {
    	                    view.loadUrl(url);               
    	                    return true;
    					}});
    				wv.loadUrl("http://www.kth.se/");
    				midsection.addView(wv);
    			}*/
    			
    			Log.v("blechristmas", "tree");
    			break;
    		case 0:
    			//change-View
    			/*setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    			midsection.removeAllViews();
    			vv = new VideoView(this);
    			vv.setVideoURI(Uri.parse("http://msl1.it.kth.se/videos/JensHoHoHo.mp4"));
    			vv.setMediaController(new MediaController(this));
    			vv.requestFocus();
    			midsection.addView(vv);
    			vv.start();
    			vv.setOnCompletionListener(new MediaPlayer.OnCompletionListener() 
    			{           
    				public void onCompletion(MediaPlayer mp) 
    				{
    					//TODO after completion of song
    					vv.setVisibility(View.INVISIBLE);
    					}           
    				});*/
    			
    			//TODO play-christmas-video
    			vv.setVisibility(View.VISIBLE);
    			vv.start();
    			vv.setOnCompletionListener(new MediaPlayer.OnCompletionListener() 
    			{           
    				public void onCompletion(MediaPlayer mp) 
    				{
    					vv.setVisibility(View.INVISIBLE);
    					}           
    				});

    			Log.v("blechristmas", "tree");
    			break;
    		/*case 1:
    			break;
    		case 2:
    			break;
    		case 3:
    			break;*/
    		default:break;
    			
    	}
    }
    /*private void setColor(int num) {
    	switch (num) {
    	case -1: 
    		ible_ibt0.setBackgroundColor(Color.LTGRAY);
    		ible_ibt1.setBackgroundColor(Color.LTGRAY);
    		ible_ibt2.setBackgroundColor(Color.LTGRAY);
    		ible_ibt3.setBackgroundColor(Color.LTGRAY);
    		break;
    	case 0: 
    		ible_ibt0.setBackgroundColor(Color.GREEN);
    		ible_ibt1.setBackgroundColor(Color.LTGRAY);
    		ible_ibt2.setBackgroundColor(Color.LTGRAY);
    		ible_ibt3.setBackgroundColor(Color.LTGRAY);
    		break;
    	default: 
    		ible_ibt0.setBackgroundColor(Color.LTGRAY);
    		ible_ibt1.setBackgroundColor(Color.LTGRAY);
    		ible_ibt2.setBackgroundColor(Color.LTGRAY);
    		ible_ibt3.setBackgroundColor(Color.LTGRAY);
    		break;
    	}
    }    */
    
    public String sendConnectGet(String url0) throws Exception {
		try{
			URL obj = new URL(url0);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("GET");
			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'GET' request to URL : " + url0);
			System.out.println("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			return response.toString();
		}catch(Exception ex){
			return "";
		}
	}
    
}