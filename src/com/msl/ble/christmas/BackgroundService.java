package com.msl.ble.christmas;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.Log;

public class BackgroundService extends Service {
	String TAG = "ServerUploadTask";
	Context context;

	public BackgroundService(Context context) {
		this.context = context;
		
		Log.v("blerun", "+++ started");
		SystemClock.sleep(5*1000);
		Intent it = new Intent(this.context, COS_ChristmasTree.class);
		it.addCategory(Intent.CATEGORY_HOME);
		it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		this.context.startActivity(it);
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

}
